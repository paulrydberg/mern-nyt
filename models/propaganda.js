const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create skiii-ma
const FakeNewsSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  comments: {
    type: String
  },
  saved: false,
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Propaganda = mongoose.model('propaganda', FakeNewsSchema);
