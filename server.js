const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
var PORT = process.env.PORT || 5000;

//
const items = require('./routes/api/items');

const app = express();

// body parser middleware
app.use(bodyParser.json());

//db config
const db = require('./config/keys').mongoURI;

//connect to mongo
mongoose
  .connect(db)
  .then(() => console.log('mongoDB Connected :)'))
  .catch(err => console.log(err));

app.use('/api/items', items);

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
